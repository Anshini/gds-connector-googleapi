/** Define namespace */
var connector = connector || {};

/** @const */
connector.API_KEY = 'AIzaSyB_3Udcprm-IijVCkz1Q8Ivixu2P-dOB6M';

/**
* Apps Script Cache expiration time (in seconds) for UrlFetch response.
* @const
*/
connector.cacheExpiration = 1 * 60;

/** @const */
connector.cacheTag = 'ulrFetch-results';

/** @const */
connector.logEnabled = false;

var service = undefined;
/**
* Wrapper function for `connector.getAuthType()`
* @returns {Object} `AuthType` used by the connector.
*/
function getAuthType() {
  return connector.logAndExecute('getAuthType', null);
}

/**
* Returns the authentication method required by the connector to authorize the
* third-party service.
* Required function for Community Connector.
* @returns {Object} `AuthType` used by the connector.
*/
connector.getAuthType = function () {
  var response = { 'type': 'OAUTH2' };
  return response;
};

function getOAuthService() {
var scriptProperties = PropertiesService.getScriptProperties();
var initSequence =  scriptProperties.getProperty('INIT_SEQUENCE');
  if(initSequence == null){
       scriptProperties.setProperty('INIT_SEQUENCE', true);
       scriptProperties.setProperty('CLIENT_ID', "bc22p5vM48k1MCc1vE9d");
       scriptProperties.setProperty('CLIENT_SECRET', "op[5Rk#)T|5~zZwiQ]9tG44>N3@FTiIf");
       scriptProperties.setProperty('TOKEN_URL', "http://didier.taponix.com/server/api/oauth/access_token");
       scriptProperties.setProperty('TOKEN_URL_PATH', "oauth/access_token");
       scriptProperties.setProperty('SERVICES_OR_CATEGORIES', '%serviceOrCategory%');
       scriptProperties.setProperty('SERVICE_ID', '%serviceId%');
       scriptProperties.setProperty('ICON', "https://www.e-nor.com/wp-content/uploads/2017/06/360-datastudio_icon.png");
       scriptProperties.setProperty('GDS_CONNECTOR_API_BASE_URL', "http://tapclicks-test.apigee.net/");
       scriptProperties.setProperty('AUTHORIZATION_BASE_URL', "https://oauthproxy.tapclicks.net/");     
  }
  if(!service) {
    var oauthProperties = this.getOauthProperties();
    return OAuth2.createService('tapApi')
    .setAuthorizationBaseUrl(oauthProperties.authorizationBaseUrl)
    .setTokenUrl(oauthProperties.tokenUrl)
    .setClientId(oauthProperties.clientId)
    .setClientSecret(oauthProperties.ClientSecret)
    .setPropertyStore(PropertiesService.getUserProperties())
    .setCallbackFunction('authCallback')
    .setParam('icon',oauthProperties.icon);
  } else {
    return service;
  }
};

/**
* Wrapper function for `connector.getConfig()`.
*
* @param {Object} request - Config request parameters.
* @returns {Object} Connector configuration to be displayed to the user.
*/
function getConfig(request) {
  return connector.logAndExecute('getConfig', request);
}

connector.getConfig = function(request) {
  var serviceDetails = getServiceDetails();
  var accessToken = getAccessToken();
  var dataViews = getDataViews(serviceDetails.type, serviceDetails.id, accessToken.accessToken);
  var clients = getClients(accessToken.accessToken);
  var info = "";
  if(dataViews==null && clients==null){
    info = info + "Not Able to Retrieve Data From Server Currently. Please Try by Logging in Again";
  }
  else{
    info = info + "It is Mandatory to select Atleast one value for Both Dataview and Client";
  }
  var config = {
    scriptParams: {
      "sampleExtraction": true,
    },
    configParams: [
      {
        "type": "SELECT_SINGLE",
        "name": "dataViews",
        "displayName": "*Select Data Views",
        "helpText": "Select Single Dataview",
        "options": dataViews
      },
      {
        "type": "SELECT_MULTIPLE",
        "name": "customerID",
        "displayName": "*Select Clients or Client Groups",
        "helpText": "Select Aleast One Client or Client Group",
        "options": clients
      },
      {
        "type": "INFO",
        "name": "displayMessage",
        "text": info
      }
    ],
    "dateRangeRequired": true
  };
  return config;
}

/**
* Wrapper function for `connector.getSchema()`.
*
* @param {Object} request - Schema request parameters.
* @returns {Object} Schema for the given request.
*/

function getSchema(request) {
  return connector.logAndExecute('getSchema', request);
}

/**
* Returns the schema for the given request.
*
* @param {Object} request - Schema request parameters.
* @returns {Object} Schema for the given request.
*/
connector.getSchema = function(request) {
  var serviceDetails = getServiceDetails();
  var accessToken = getAccessToken();
  if(request.configParams.dataViews && request.configParams.customerID){
    var schema = createSchema(serviceDetails.type, serviceDetails.id, accessToken.accessToken, request.configParams.dataViews, request.configParams.customerID);  
    return {schema: schema};
  }
};


function isAdminUser() {
  // CS: Set to false
  return false;
}

/**
* Wrapper function for `connector.getData()`.
*
* @param {Object} request - Data request parameters.
* @returns {Object} Contains the schema and data for the given request.
*/
function getData(request) {
  return connector.logAndExecute('getData', request);
}


/**
* Returns the tabular data for the given request.
*
* @param {Object} request - Data request parameters.
* @returns {Object} Contains the schema and data for the given request.
*/
connector.getData = function(request) {
  var dataSchema = [];
  // CS: No validations/error handling
  var schemaProp = PropertiesService.getUserProperties().getProperty('schema');
  var schema = JSON.parse(schemaProp);
  request.fields.forEach(function(field) {
    for (var i = 0; i < schema.length; i++) {
      if (schema[i].name === field.name) {
        dataSchema.push(schema[i]);
        break;
      }
    }
  }); 
  var daterange = request.dateRange.startDate + "%7C" + request.dateRange.endDate;
  var url =  PropertiesService.getUserProperties().getProperty('url');
  url = url + daterange;
  var accessToken = getAccessToken();
  var headers =
      {
        "Authorization" : "Bearer "+ accessToken.accessToken,
      };
  var options =
      {
        "method"  : "GET",
        "headers" : headers,
        "followRedirects" : true,
        "muteHttpExceptions": true
      };
    var result = UrlFetchApp.fetch(url, options);
    if (result.getResponseCode() == 200){
      var columns = JSON.parse(result.getContentText());
      result = columns.data; 
      var data = [];
      for(var i = 0 ; i < result.length;i++){  
        var values = [];
        for(var j = 0 ; j < dataSchema.length;j++){
          var field = dataSchema[j].name;
          if(result[i] && field in result[i]) {
            var value = result[i][field];
            values.push(value);
          }
        }
        data.push({
          values: values
        });
      };  
      return {
        schema: dataSchema,
        rows: data
      };
    }
}

function authCallback(request) {
  var service = getOAuthService();
  var tokenUrlPath = getTokenUrlPath();
  if(!request.parameter.instance){
    return HtmlService.createHtmlOutput('Error in authentication. Please try again later');
  }
  else{
    var baseUrl = request.parameter.instance + '/server/api/';
    PropertiesService.getUserProperties().setProperty('GDS_CONNECTOR_API_BASE_URL', baseUrl); 
    service.setTokenUrl(baseUrl + tokenUrlPath.urlPath);
    var authorized = service.handleCallback(request);
    if (authorized) {
      var now = new Date();
      var expiryTime = service.token_.data.expires_in * 1000;
      var saveTime = new Date(now.getTime() + expiryTime);
      var tokenDetails = {
        user : Session.getEffectiveUser().getEmail(),
        accessToken : service.token_.data.access_token,
        refreshToken: service.token_.data.refresh_token,
        lastUpdatedTime :saveTime
      };
      PropertiesService.getUserProperties().setProperty('USER_PROPERTIES', JSON.stringify(tokenDetails));
      var app = UiApp.createApplication();
      return HtmlService.createHtmlOutputFromFile('successPage');
    } else {
      return HtmlService.createHtmlOutputFromFile('deniedPage');
    }
  }
}

function isAuthValid() {
  var service = getOAuthService();
  if (service == null) {
    return false;
  }
  return service.hasAccess();
}

function get3PAuthorizationUrls() {
  var service = getOAuthService();
  return service.getAuthorizationUrl();
}

function resetAuth() {
  var service = getOAuthService()
  service.reset();
}

function replenishRefreshToken()
{
  var tokenPathUrl = getTokenUrlPath();
  var service = getOAuthService();
  var oauthProperties = this.getOauthProperties();
  var user =  PropertiesService.getUserProperties().getProperty('USER_PROPERTIES');  
  var userData = JSON.parse(user);
  var payload =
      {
        "grant_type" : "refresh_token",
        "client_id" : oauthProperties.clientId,
        "client_secret" : oauthProperties.ClientSecret,
        "refresh_token" : userData.refreshToken
      };
  var options =
      {
        "method"  : "POST",
        "payload" : payload,
        "contentType": "application/x-www-form-urlencoded",
        "followRedirects" : true,
        "muteHttpExceptions": true
      };   
  var baseUrl = PropertiesService.getUserProperties().getProperty('GDS_CONNECTOR_API_BASE_URL'); 
  var url = baseUrl + tokenPathUrl.urlPath;
  try{
    var  response = UrlFetchApp.fetch(url, options);
    if(response.getResponseCode() == 200){
      PropertiesService.getUserProperties().deleteProperty('USER_PROPERTIES');
      response = JSON.parse(response.getContentText());
      var now = new Date();
      var expiryTime = response.data.expires_in * 1000;
      var saveTime = new Date(now.getTime() + expiryTime);
      var tokenDetails = {
        user : Session.getEffectiveUser().getEmail(),
        accessToken : response.data.access_token,
        refreshToken: response.data.refresh_token,
        lastUpdatedTime :saveTime
      };
      PropertiesService.getUserProperties().setProperty('USER_PROPERTIES', JSON.stringify(tokenDetails));  
    }  
    else {
      if(response.getResponseCode() == 401){
        throw new Error("Unauthorized : The page you were trying to access cannot be loaded until you log in again");
      }
      else{
        throw new Error("Internal Server Error");
      }   
    }
  }catch(e){
    connector.throwError(e,true);
  }  
}

connector.throwError = function (message, userSafe) {
  if (userSafe) {
    message = 'DS_USER:' + message;
  }
  throw new Error(message);
};

/**
* Stringifies parameters and responses for a given function and logs them to
* Stackdriver.
*
* @param {string} functionName - Function to be logged and executed.
* @param {Object} parameter - Parameter for the `functionName` function.
* @returns {any} Returns the response of `functionName` function.
*/
connector.logAndExecute = function(functionName, parameter) {
  if (connector.logEnabled && connector.isAdminUser()) {
    var paramString = JSON.stringify(parameter, null, 2);
    console.log([functionName, 'request', paramString]);
  }
  var returnObject = connector[functionName](parameter);
  if (connector.logEnabled && connector.isAdminUser()) {
    var returnString = JSON.stringify(returnObject, null, 2);
    console.log([functionName, 'response', returnString]);
  }
  return returnObject;
};
