// ******* GET DATA VIEWS - for getConfig function ******* 
function getDataViews(serviceId, dataId, accessToken) {
  var apiBaseUrl = getGdsConnectorApiBaseUrl();
  var url = apiBaseUrl + serviceId + "/" + dataId + "/data/data_views";
  var headers =
      {
        "Authorization" : "Bearer "+ accessToken,
      };
  var options =
      {
        "method"  : "GET",
        "headers" : headers,   
        "followRedirects" : true,
        "muteHttpExceptions": true
      };
  try{
    var result = UrlFetchApp.fetch(url, options);
    if (result.getResponseCode() == 200) {
      var params = JSON.parse(result.getContentText());  
      var data = params.data;
      var dataViews = [];
      for(i in data){
        var obj = { 
          "label": data[i].name,
          "value": data[i].id
        };
        dataViews.push(obj);
      }
      return dataViews;
    }
    else
    {
      if(result.getResponseCode() == 401){
        throw new Error("Unauthorized : The page you were trying to access cannot be loaded until you log in again");
      }
      else{
        throw new Error("Internal Server Error");
      }   
    }
  }catch(e){
    connector.throwError(e,true);
  }
};

// ******* GET CLIENTS - for getConfig function ******* 
function getClients(accessToken){
  var apiBaseUrl = getGdsConnectorApiBaseUrl();
  var url = apiBaseUrl +"clients";
  var headers =
      {
        "Authorization" : "Bearer "+ accessToken,
      };
  var options =
      {
        "method"  : "GET",
        "headers" : headers,   
        "followRedirects" : true,
        "muteHttpExceptions": true
      };
  try{
    var result = UrlFetchApp.fetch(url, options);
    if (result.getResponseCode() == 200) {
      var params = JSON.parse(result.getContentText());  
      var data = params.data;
      var clients = [];
      for(i in data){
        var obj = { 
          "label": data[i].company_name,
          "value": data[i].id
        };
        clients.push(obj);
      }
      var clientsAndClientGroups = getClientGroups(clients, accessToken);
      return clientsAndClientGroups;
    } 
    else
    {
      if(result.getResponseCode() == 401){
        throw new Error("Unauthorized : The page you were trying to access cannot be loaded until you log in again");
      }
      else{
        throw new Error("Internal Server Error");
      }   
    }
  }catch(e){
    connector.throwError(e,true);
  }
};



// ******* GET CLIENT GROUPS - for getConfig function ******* 
function getClientGroups(clientAndClientGroups, accessToken){
  var apiBaseUrl = getGdsConnectorApiBaseUrl();
  var url = apiBaseUrl + "clientgroups";
  var headers =
      {
        "Authorization" : "Bearer "+ accessToken,
      };
  var options =
      {
        "method"  : "GET",
        "headers" : headers,   
        "followRedirects" : true,
        "muteHttpExceptions": true
      };
  try{
    var result = UrlFetchApp.fetch(url, options);
    var params = JSON.parse(result.getContentText()); 
    if (result.getResponseCode() == 200 && params.data) { 
      var data = params.data;
      for(i in data){
        var obj = { 
          "label": data[i].name,
          "value": data[i].id
        };
        clientAndClientGroups.push(obj);
      }
      return clientAndClientGroups;
    }
    else
    {
      if(result.getResponseCode() == 401){
        throw new Error("Unauthorized : The page you were trying to access cannot be loaded until you log in again");
      }
      else{
        throw new Error("Internal Server Error");
      }   
    }
  }catch(e){
    connector.throwError(e,true);
  }
};

// Function to create Schema - for getSchema function ******* 
function createSchema(serviceId, dataId, accessToken, dataViews, customerID){
  var apiBaseUrl = getGdsConnectorApiBaseUrl();
  var fields ="";
  var dataSchema = [];
  var url = apiBaseUrl + serviceId + "/" + dataId + "/data/" + dataViews + "?metadata=1";
  var headers =
      {
        "Authorization" : "Bearer "+ accessToken,
      };
  var options =
      {
        "method"  : "GET",
        "headers" : headers,   
        "followRedirects" : true,
        "muteHttpExceptions": true
      };
    var result = UrlFetchApp.fetch(url, options);
    if (result.getResponseCode() == 200) {
      var columns = JSON.parse(result.getContentText()); 
      columns = columns.metadata.columns;
      for(var value in columns){
        fields+=columns[value].field;
        if (value != columns.length - 1) {
          fields += ","
        }       
        var dataType;
        switch (columns[value].format) {
          case "integer": 
            dataType = "NUMBER";
            break;
          case "decimal":
            dataType = "NUMBER";
            break;
          case "percent":
            dataType = "NUMBER";
            break;
          case "currency":
            dataType = "NUMBER";
            break;
          case "string":
            dataType = "STRING";
            break;
          case "link":
            dataType = "STRING";
            break;
          case "datetime":
            dataType = "STRING";
            break;
          case "time":
            dataType = "NUMBER";
            break;
          case "bool":
            dataType = "BOOLEAN";
            break;
          case "id":
            dataType = "NUMBER";
            break;
          case "image":
            dataType = "STRING";
            break;
          default :
            dataType = "STRING";
            break;
        };
        
        var conceptType;
        if (columns[value].is_metric) {
          conceptType = "METRIC";
        } else {
          conceptType = "DIMENSION";
        };  
        
        dataSchema[value] = {
          "name": columns[value].field,
          "label": columns[value].label,
          "dataType": dataType,
          "semantics": {
            "conceptType": conceptType
          }
        };    
      }
      PropertiesService.getUserProperties().setProperty('schema', JSON.stringify(dataSchema));
      url = apiBaseUrl + serviceId + "/" + dataId + "/data/" + dataViews + "?fields=" + fields + "&customer_id="+ customerID + "&timegrouping=hourly&daterange=";
      PropertiesService.getUserProperties().setProperty('url', url);
      return dataSchema;
    }
};