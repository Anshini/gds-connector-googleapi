// ******* Accessing values for Oauth authentication - getOAuthService()  *******
function getOauthProperties(){
 var scriptProperties = PropertiesService.getScriptProperties();
 var oauthProperties = {
   authorizationBaseUrl : scriptProperties.getProperty("AUTHORIZATION_BASE_URL"),
   clientId  : scriptProperties.getProperty("CLIENT_ID"),
   ClientSecret : scriptProperties.getProperty("CLIENT_SECRET"),
   tokenUrl :  scriptProperties.getProperty("TOKEN_URL"),
   icon : scriptProperties.getProperty("ICON")
 };
 return oauthProperties;
}

//  ******* Function to Access ACCESS_TOKEN  *******
function getAccessToken() {
  var token = getOAuthService().getAccessToken();
  if(token) { 
    return token;
  }
   var user =  PropertiesService.getUserProperties().getProperty('USER_PROPERTIES');
   var userData = JSON.parse(user);
   if(userData.user == Session.getEffectiveUser().getEmail()){
      var now = new Date();
      var expirytime = new Date(userData.lastUpdatedTime);
      if(now.valueOf() > expirytime.valueOf()){
       var tokenPathUrl = getTokenUrlPath();
       var service = getOAuthService();
       var oauthProperties = this.getOauthProperties();
       var payload =
       {
        "grant_type" : "refresh_token",
        "client_id" : oauthProperties.clientId,
        "client_secret" : oauthProperties.ClientSecret,
        "refresh_token" : userData.refreshToken
       };
      var options =
      {
        "method"  : "POST",
        "payload" : payload,
        "contentType": "application/x-www-form-urlencoded",
        "followRedirects" : true,
        "muteHttpExceptions": true
      };   
    var baseUrl = PropertiesService.getUserProperties().getProperty('GDS_CONNECTOR_API_BASE_URL'); 
    var url = baseUrl + tokenPathUrl.urlPath; 
    try{
      var  response = UrlFetchApp.fetch(url, options);
      if(response.getResponseCode() == 200){
      PropertiesService.getUserProperties().deleteProperty('USER_PROPERTIES');
        response = JSON.parse(response.getContentText());
        var now = new Date();
        var expiryTime = response.data.expires_in * 1000;
        var saveTime = new Date(now.getTime() + expiryTime);
        var tokenDetails = {
          user : Session.getEffectiveUser().getEmail(),
          accessToken : response.data.access_token,
          refreshToken: response.data.refresh_token,
          lastUpdatedTime :saveTime
        };
        PropertiesService.getUserProperties().setProperty('USER_PROPERTIES', JSON.stringify(tokenDetails));
        return { accessToken : tokenDetails.accessToken};
      }
      else{
      if(response.getResponseCode() == 401){
        throw new Error("Unauthorized : The page you were trying to access cannot be loaded until you log in again");
      }
      else{
        throw new Error("Internal Server Error");
      }   
    } 
    }catch(e){
      connector.throwError(e,true);;
    }
  }
  else{
    return {accessToken : userData.accessToken};
  }
 } 
}

//  ******* Function to Access Tapclicks base url to fetch data  *******
function getGdsConnectorApiBaseUrl() {
  return PropertiesService.getUserProperties().getProperty("GDS_CONNECTOR_API_BASE_URL");
}


//  ******* Functio to Access Service/Category ID for a Connector, which is a unique value for each connector  *******
function getServiceDetails(){
 var serviceProperty = PropertiesService.getScriptProperties();
 var service = {
   type : serviceProperty.getProperty("SERVICES_OR_CATEGORIES"),
   id : serviceProperty.getProperty("SERVICE_ID")
 };
 return service;
}

//  ******* Function to Get Token Url Path  *******
function getTokenUrlPath() {
  return {urlPath : PropertiesService.getScriptProperties().getProperty("TOKEN_URL_PATH")}
}
