var fs = require('fs');
var google = require('googleapis');
var readline = require('readline');
var googleAuth = require('google-auth-library');
var _ = require('underscore');
var rl = require("readline");

var prompts = rl.createInterface(process.stdin, process.stdout);
var code = "";
var tap_api = "";
var accessScriptProperties = "";
var script_file = "";
var successPage = "";
var deniedPage = "";

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/script-nodejs-quickstart.json
var SCOPES = ['https://www.googleapis.com/auth/script.projects'];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'script-nodejs-quickstart.json';

// Load client secrets from a local file.
fs.readFile('config/client_secret.json', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading client secret file: ' + err);
    return;
  }
  // Authorize a client with the loaded credentials, then call the
  // Google Apps Script API.
  authorize(JSON.parse(content), callAppsScript);
});


fs.readFile('appScript/Code.gs', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading Code.gs file: ' + err);
    return;
  }
  // Authorize a client with the loaded credentials, then call the
  // Google Apps Script API.
   code = content.toString('utf8');
});

fs.readFile('appScript/tapApi.gs', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading tapApi.gs file: ' + err);
    return;
  }
  // Authorize a client with the loaded credentials, then call the
  // Google Apps Script API.
  tap_api = content.toString('utf8');

});

fs.readFile('appScript/accessScriptProperties.gs', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading accessScriptProperties.gs file: ' + err);
    return;
  }
  // Authorize a client with the loaded credentials, then call the
  // Google Apps Script API.
  accessScriptProperties = content.toString('utf8');

});
  
fs.readFile('appScript/appsscript.json', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading appScript file: ' + err);
    return;
  }
  script_file = content.toString('utf8');
});

fs.readFile('appScript/successPage.html', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading appScript file: ' + err);
    return;
  }
 successPage = content.toString('utf8');
});

fs.readFile('appScript/deniedPage.html', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading appScript file: ' + err);
    return;
  }
 deniedPage = content.toString('utf8');
});



/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  var clientSecret = credentials.installed.client_secret;
  var clientId = credentials.installed.client_id;
  var redirectUrl = credentials.installed.redirect_uris[0];
  var auth = new googleAuth();
  var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

  // Check if we have previously stored a token.
  console.log("TOKEN PATH : " + TOKEN_PATH);
  fs.readFile(TOKEN_PATH, function(err, token) {
    if (err) {
      getNewToken(oauth2Client, callback);
    } else {
      oauth2Client.credentials = JSON.parse(token);
      callback(oauth2Client);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
  var authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url: ', authUrl);
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', function(code) {
    rl.close();
    oauth2Client.getToken(code, function(err, token) {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token);
      callback(oauth2Client);
    });
  });
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code != 'EEXIST') {
      throw err;
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(token));
  console.log('Token stored to ' + TOKEN_PATH);
}


var services = [{
    "service_id": "44",
    "name": "Facebook Insights",
    "row_grouping_count": "1",
    "type" : "services"
  },
  {
    "service_id": "179",
    "name": "RTBiQ",
    "row_grouping_count": "1",
    "type" : "services"
  },
  {
    "service_id": "33",
    "name": "Google Analytics",
    "row_grouping_count": "1",
    "type" : "services"
  }];

/**
 * Shows basic usage of the Apps Script API.
 *
 * Call the Apps Script API to create a new script project, upload a file to the
 * project, and log the script's URL to the user.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function callAppsScript(auth) {
  let script = google.script('v1');
  _.each(services, function(service){
    var name = service.name;
    var codeData = code;
    createAppScript(name, function(appScript){
      script.projects.create({
        auth,
        resource: {
            title: name
          }
        }, {}, (err, res) => {
        if (err) {
        // The API encountered a problem.
          return console.log(`The API returned an error in create: ${err}`);
        }
        codeData = codeData.replace('%serviceId%', service.service_id);
        codeData = codeData.replace('%serviceOrCategory%', service.type);
        script.projects.updateContent({
          scriptId: res.scriptId,
          auth,
          resource: {
            files: [{
                name: 'Code',
                type: 'SERVER_JS',
                source: codeData
              },{
                name: 'appsscript',
                type: 'JSON',
                source: appScript
              },{
                name: 'accessScriptProperties',
                type: 'SERVER_JS',
                source: accessScriptProperties
              },
              {
                name: 'tapapi',
                type: 'SERVER_JS',
                source: tap_api
              },
               {
                name: 'successPage',
                type: 'HTML',
                source: successPage
              }, {
                name: 'deniedPage',
                type: 'HTML',
                source: deniedPage
              }]
            }
          }, {}, (err, res) => {
          if (err) {
            // The API encountered a problem.
            return console.log(`The API returned an error in update: ${err}`);
          }
          console.log(`https://script.google.com/d/${res.scriptId}/edit`);
          return true;
        });
      });
    }); 
  });
}


function createAppScript(name, callback){
  var accessScript = script_file;
  fs.readFile('config/connectors.json', function processClientSecrets(err, content) {
    if (err) {
      console.log('Error loading client secret file: ' + err);
      return;
    }
    var data = JSON.parse(content);
    for(var index in data){
      if(data[index].name == name){
        accessScript = "\n" + accessScript + JSON.stringify(data[index]) + "}";
        callback(accessScript);
      }
    }
  });
}


